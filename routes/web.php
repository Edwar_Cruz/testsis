<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SaleController;
use App\Http\Controllers\MailController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/logout', [LoginController::class,'logout'])
    ->name('logout')
    ->middleware('auth');

Route::get('/login', [LoginController::class,'create'])
    ->name('login.create')
    ->middleware('guest');

Route::post('/login', [LoginController::class,'login'])
    ->name('login.login');

Route::get('/register', [RegisterController::class,'create'])
    ->name('register.create')
    ->middleware('guest');
      
Route::post('/register', [RegisterController::class,'store'])
    ->name('register.store');


Route::get('shop', [ShopController::class, 'create'])
    ->name('shop.create')
    ->middleware('auth') ;

Route::get('shop/create', [ShopController::class, 'formCreate'])
    ->name('shop.formCreate')
    ->middleware('auth') ;


Route::get('user', [UserController::class, 'index'])
    ->name('user.index')
    ->middleware('auth');

Route::get('user/{user_id}', [UserController::class, 'delete'])
    ->name('user.delete')
    ->middleware('auth');

Route::get('user/update/{user_id}', [UserController::class, 'index'])
    ->name('user.update')
    ->middleware('auth');

Route::post('user/edit', [UserController::class, 'update'])
    ->name('user.edit')
    ->middleware('auth');

Route::post('user', [UserController::class, 'store'])
    ->name('user.store')
    ->middleware('auth');

Route::get('shopping/{success?}', [SaleController::class, 'index'])
    ->name('shopping.index')
    ->middleware('auth');

Route::post('sales', [SaleController::class, 'store'])
    ->name('sales.store')
    ->middleware('auth');

//remove Sale
Route::get('sales/{produtc_id}', [SaleController::class, 'removeProduct'])
    ->name('sale.remove')
    ->middleware('auth');


/**
 * Envio de send
 */
Route::get('send-email', [MailController::class, 'SendEmails'])
    ->name('email')
    ->middleware('auth');