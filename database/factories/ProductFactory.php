<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'product' => $this->faker->word(),
            'stock' => $this->faker->numberBetween($min = 1, $max = 50),
            'price' => 10000,
            'image' => $this->faker->imageUrl($width = 640, $height = 480)
        ];
    }
}
