<nav class="navbar navbar-expand-lg text-light bg-primary">
    <div class="container">
      {{-- <a class="navbar-brand" href="#">Navbar</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button> --}}
      <div class="collapse navbar-collapse " id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link text-light" aria-current="page" href="{{ route('shop.create') }}">Productos</a>
          </li>
          @if (auth()->user()->role == 'Vendedor')
              
              <li class="nav-item">
                <a class="nav-link text-light" href=" {{ route('user.index')}} ">Usuarios</a>
              </li>
            @endif
          </ul>
        <div class="d-flex">
            <div class="text-align-left mx-5">
               ¡Bienvenido <b> {{ auth()->user()->name }} </b>
               <br>
               <a href=" {{route('logout') }} ">
                   <span class="badge bg-warning text-dark">Cerrar Sesión</span>
               </a>
            </div>

            <a href="{{route('shopping.index')}}">
              <button class="btn btn-outline-light">
                  Carrito
                  @if ($number)
                    <span class="badge bg-danger">{{$number}}</span>  
                  @endif
              </button>
            </a>

        </div>
      </div>
    </div>
  </nav>