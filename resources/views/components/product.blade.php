<div class="card" style="width: 100%;">
    <form action="{{ route('sales.store')}}" method="POST">
        @csrf
        <img src="{{$image}}" class="card-img-top" alt="..." >
        <div class="card-body text-center">
            <h5 class="card-title ">{{$product}}</h5>
            <p class="card-text">
                Precio: {{ $price}} <br>
                <span class="badge rounded-pill bg-success">Disponibles: <b> {{$stock}} </b></span>
            </p>
            <div class="row justify-content-center justify-content-md-start ">
                <div class="col">
                    <input type="number" name="amount" min="1" max="{{$stock}}" class="form-control" value="1">
                    <input type="hidden" name="price" class="form-control" value="{{$price}}">
                    <input type="hidden" name="product_id" class="form-control" value="{{$product_id}}">

                </div>
                <div class="col">
                    <button type="submit" class="btn btn-outline-info">Añadir</button>
                </div>
            </div>
        </div>
    </form>
    {{-- @if (auth()->user()->role == "Vendedor")
        <a href="" class="text-center">
            <span class="badge bg-warning text-dark">Editar</span>
        </a>
    @endif --}}
    {{-- <button class="btn btn-outline-info">Editar</button> --}}
  </div>