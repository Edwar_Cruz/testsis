<form method="POST" action=" {{route($action)}} ">
    @csrf
    <div class="mb-3">
      <input type="hidden" name="id" value="{{$user->id}}">
      <label for="exampleInputName1" class="form-label">Nombre</label>
      <input type="text" name="name" required class="form-control" id="exampleInputName1" value="{{$user->name}}">
      @error('name')
        <div class="alert alert-danger d-flex align-items-center" role="alert">
            {{ $message}}
        </div>    
      @enderror
    </div>
    <div class="mb-3">
      <label for="exampleInputEmail1" class="form-label">Dirección de correo</label>
      <input type="email" name="email" disabled class="form-control" id="exampleInputEmail1" value="{{$user->email}}">
      @error('email')
        <div class="alert alert-danger d-flex align-items-center" role="alert">
            {{ $message}}
        </div>    
      @enderror
    </div>

    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Seleccione su rol</label>
        <select class="form-select" required name="role" aria-label="Default select example" value="{{$user->role}}">
            {{-- <option selected>Seleccionar..</option> --}}
            <option value="Comprador" selected>Comprador</option>
            <option value="Vendedor">Vendedor</option>
        </select>
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword1" class="form-label">Password</label>
      <input type="password" name="password" required class="form-control" id="exampleInputPassword1">
      @error('password')
        <div class="alert alert-danger d-flex align-items-center" role="alert">
            {{ $message }}
        </div>    
      @enderror
    </div>
    <div class="mb-3">
      <label for="exampleInputPassword2" class="form-label">Confirmar Password</label>
      <input type="password" name="password_confirmation" required class="form-control" id="exampleInputPassword2">
    </div>
    <button type="submit" class="btn btn-primary">Actualizar</button>
  </form>