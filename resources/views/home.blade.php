@extends('Layout.app')

@section('title','Home')

@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-center mt-5 p-5">
                ¡Bienvenidos!
            </h1>
            <p class="text-center">
                La mejor tienda de ventas en el mundo
            </p>
        </div>
    </div>    
    <div class="row justify-content-center">
        <div class="col-2  text-center">
            <a href="{{route('login.create') }}" class="btn btn-primary"> Iniciar Sesión</a>
        </div>
        <div class="col-2 text-center">
            <a href="{{route('register.create') }}" class="btn btn-light"> Registrarse</a>
        </div>
    </div>

@endsection