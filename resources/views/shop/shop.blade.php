@extends('Layout.app')

@section('title','Shop')

@section('content')
    <x-nav number=1></x-nav>
    <div class="container">
        <div class="row justify-content-center">
            @foreach ($products as $product)
                <div class="col-3 m-3">
                    {{-- {{$product}} --}}
                    <x-product product="{{$product->product}}" 
                        price="{{$product->price}}" 
                        stock="{{ $product->stock }}" 
                        image="{{ $product->image }}"
                        id="{{ $product->id }}"
                    ></x-product>
                </div>
                
            @endforeach
        </div>
    </div>
@endsection