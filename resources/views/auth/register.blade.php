@extends('Layout.app')

@section('title','Register')

@section('content')
    <x-btnclose/>

    <div class="row justify-content-center m-0 vh-100 align-items-center bg-light">
        <div class="col-auto">
            <div class="card mb-3 " style="max-width: 640px;">
                <div class="row g-0">
                  <div class="col-md-6 justify-content-center m-0 align-items-center">
                    <img src="https://img.freepik.com/vector-gratis/crecimiento-ingresos-freelancer-sentado-monedas-trabajando-personaje-dibujos-animados-portatil-ganancia-dinero-ventas-virtuales-estrategia-marketing-ilustracion-metafora-concepto-aislado-vector_335657-2755.jpg?size=338&ext=jpg" class="img-fluid rounded-start" alt="...">
                  </div>
                  <div class="col-md-6">
                    <div class="card-body">
                      <h3 class="card-title font-bold">Formulario de registro</h3>
                      <p class="card-text">
                        <x-formregister action="register.create" />   
                      </p>
                      <p class="card-text">
                          <small class="text-muted">Ya estoy registrado, deseo <a href="{{ route('login.create') }}">iniciar sesión</a></small>
                        </p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection