@extends('Layout.app')

@section('title','Login')

@section('content')
    
    <x-btnclose/>


    <div class="row justify-content-center m-0 vh-100 align-items-center bg-light">
        <div class="col-auto">
            <div class="card mb-3 " style="max-width: 640px;">
                <div class="row g-0">
                  <div class="col-md-6 justify-content-center m-0 align-items-center">
                    <img src="https://img.freepik.com/vector-gratis/smm-promocion-internet-publicidad-online-anuncio-investigacion-mercado-crecimiento-ventas-comercializador-personaje-dibujos-animados-portatil-altavoz-ilustracion-metafora-concepto-aislado-vector_335657-2849.jpg?size=338&ext=jpg" class="img-fluid rounded-start" alt="...">
                  </div>
                  <div class="col-md-6">
                    <div class="card-body">
                      <h5 class="card-title">Iniciar sesión</h5>
                      <p class="card-text">
                        <form method="POST" action="">
                            @csrf
                            <div class="mb-3">
                              <label for="exampleInputEmail1" class="form-label">Dirección de correo</label>
                              <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                              <div id="emailHelp" class="form-text">Nunca compartiremos su correo electrónico con nadie más.</div>
                            </div>
                            <div class="mb-3">
                              <label for="exampleInputPassword1" class="form-label">Password</label>
                              <input type="password" name="password" class="form-control" id="exampleInputPassword1">
                            </div>

                            @error('message')
                                <div class="alert alert-danger d-flex align-items-center" role="alert">
                                    {{ $message}}
                                </div>    
                            @enderror

                            <button type="submit" class="btn btn-primary">Submit</button>
                          </form>   
                      </p>
                      <p class="card-text">
                          <small class="text-muted">Aún no tengo cuenta, deseo <a href="{{ route('register.create') }}">registrarme</a></small>
                        </p>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
@endsection