@extends('Layout.app')

@section('title','Users')

@section('content')
    <x-nav number=1></x-nav>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-5 mt-4">
                @if (Route::currentRouteName() == "user.update")
                {{-- {{ Route::currentRouteName() }} --}}
                    <h2>
                        Actualziar usuario
                    </h2>
                    <x-formupdateuser action="user.edit" user="{{$user_id}}"  />
                    <a href="{{route('user.index')}}" class="btn btn-info">Nuevo usuario</a>
                @else
                    <h2>
                        Registros de usuario
                    </h2>
                    <x-formregister action="user.store" />
                @endif
            </div>
            <div class="col-7 mt-4">

                <h2>
                    Lista de Usuarios
                </h2>
                <table class="table">
                    <thead>
                        <tr>
                            <td>ID</td>
                        <td>Nombre</td>
                        <td>email</td>
                        <td>Role</td>
                        <td>Acciones</td>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role}}</td>
                        <td>
                            <a href="{{ route('user.update',$user->id) }}">
                                <i class="fas fa-user-edit text-warning">Editar</i>
                            </a>
                            <a href="{{ route('user.delete',$user->id) }}">
                                <i class="fas fa-user-edit text-danger">Eliminar</i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection