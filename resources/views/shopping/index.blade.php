@extends('Layout.app')

@section('title','Users')

@section('content')
    <x-nav number=1></x-nav>
    <div class="container">
        @if ($success)
            <div class="alert alert-success mt-3" role="alert">
                <h4 class="alert-heading">¡Gracias por tu compra!</h4>
                <p>Fue enviado un correo de notificación</p>
                <hr>
                <p class="mb-0">Puedes serguir comprando!</p>
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-6 mt-4">
                {{-- {{count($sales)}} --}}
                @if (count($sales) < 1)

                    <div class="alert alert-info" role="alert">
                        No tienes productos agregados en el carrito de compras 
                        <br>
                        <br>
                        <div class="flex w-100">
                            <a href="{{ route('shop.create')}}  " class="btn btn-warning "> Comprar</a>
                        </div>
                    </div>

                @else

                    <h2>
                        Carrito de compras
                    </h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <td>#</td>
                                <td>Producto</td>
                                <td>Cantidad</td>
                                <td>Valor</td>
                                <td>Total</td>
                                <td>Acciones</td>
                            </tr>
                        </thead>
                        <tbody>
                        
                        @foreach ($sales as $sale)
                        <tr>
                            <td>#</td>
                            <td>{{ $sale->product }}</td>
                            <td>{{$sale->amount}}</td>
                            <td>{{ $sale->price}}</td>
                            <td>{{($sale->amount * $sale->price)}}</td>
                            <td>
                                {{-- <a href="">
                                    <i class="fas fa-user-edit text-warning">Editar</i>
                                </a> --}}
                                <a href="{{ route('sale.remove',$sale->product_id)}}">
                                    <i class="fas fa-user-edit text-danger">Quitar</i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td> <b>Total : </b>  </td>
                            <td> <b> {{  $all }} </b> </td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="row justify-content-end">
                        <div class="flex w-100">
                            <a href="{{ route('email')}}" class="btn btn-success ">Confirmar y notificar (Email)</a>
                        </div>
                        <div class="flex w-100">
                            <a href="{{ route('shop.create')}}  " class="p text-secundary">
                                <span class="badge rounded-pill bg-secondary">Seguir comprando</span>
                            </a>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection