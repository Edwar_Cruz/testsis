<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ShopController extends Controller
{
    //
    public function create()
    {
        $products = Product::where('stock','>',0)->get();
        return view('shop.shop', compact('products'));
    }
}
