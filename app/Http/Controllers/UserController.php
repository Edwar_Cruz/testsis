<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index($user_id = null)
    {
        // $user_update = ($update ? User::find($update) : -1); 
        $users = User::all();
        return view('/user.list', compact('users','user_id'));
    }

    public function delete($user_id)
    {
        $user = User::find($user_id);
        $user->delete();
        return redirect()->to('user');
    }

    public function store(Request $request)
    {
        // validated request
        $this->validate(request(),[
            'name'=>'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
            'role' => 'required'
        ]);
        
        $user = User::create(request(['name','email','password','role']));
        // Auth::login($user);
        // auth()->login($user);
        return redirect()->to('user');
    }

    public function update(Request $request)
    {
        $this->validate(request(),[
            'name'=>'required',
            'password' => 'required|confirmed',
            'role' => 'required'
        ]);
        $user = User::find($request->id);
        $user->name = $request->name;
        // $user->email = $request->email;
        $user->role = $request->role;
        $user->password = $request->password;
        $user->save();
        return redirect()->to('user');
    }
}
