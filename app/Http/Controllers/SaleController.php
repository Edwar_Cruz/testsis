<?php

namespace App\Http\Controllers;

use App\Models\Sale;
use App\Models\Product;

use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index($success = null)
    {
        $user_id = auth()->user()->id;
        if($success == true){ $this->saleProducts(); }
        $sales = Sale::whereUser_id($user_id)
                ->join('products','product_id','=','products.id')
                ->get(); 
        $all = 0;
        $success = $success;
        foreach ($sales as $key => $value) {
            $all = $all + (($value->amount * $value->price));
        }
        // return $user_id;
        // $users = User::all();
        return view('/shopping.index', compact('sales','all','success'));
    }


    /**
     * Funcion para eliminar los productos que tiene agregado un usuario
     */
    public function saleProducts()
    {
        $user_id = auth()->user()->id;
        $sales = Sale::whereUser_id($user_id)->delete(); 
    }


    public function store(Request $request)
    {
        // return $request;
        $user_id = auth()->user()->id;
        $sale = Sale::whereUser_id($user_id)->whereProduct_id($request->product_id)->first();

        if($sale){
            $sale->amount = $sale->amount + $request->amount;
            $sale->save();
        }else{
            $sale = new Sale;
            $sale->product_id = $request->product_id;
            $sale->amount = $request->amount;
            $sale->price = $request->price;
            $sale->user_id = $user_id;
            $sale->save();
        }
        $this->amountProduct($request->product_id, $request->amount);
        
        return redirect()->to('shop');
    }

    /**
     * Function para agregar o descontar las cantidades de los products
     */
    public function amountProduct($product_id, $amount)
    {
        $product = Product::find($product_id);
        $product->stock = $product->stock -  $amount;
        $product->save();
    }

    /***
     * Funcin para quitar los productos del carrito de compra
     */
    public function removeProduct($product_id)
    {
        // return $product_id;
        $user_id = auth()->user()->id;
        $sale = Sale::whereUser_id($user_id)->whereProduct_id($product_id)->first();
        $this->amountProduct($product_id, ($sale->amount*(-1)));
        $sale->delete();
        return redirect()->to('shopping');
    }
}
