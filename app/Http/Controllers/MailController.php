<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class MailController extends Controller
{
    public function sendEmails()
    {
        $details = [
            'title' => 'Compra aceptada',
            'body' => 'Puedes seguir comprado',
        ];
        Mail::to( auth()->user()->email )->send(new SendMail($details));
        $success = true;
        return redirect()->to('shopping/'.$success);
    }
}
