<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * This is a function to login user
     * Edwar Cruz 14/10/2021
     * 
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        // Auth::attempt(['email' => $email, 'password' => $password])
        if(auth()->attempt(request(['email','password'])) == false){
            return back()->withErrors([
                'message' => 'Las credenciales son incorrectas'
            ]);
        }
        return redirect()->to('shop');

    }

    /**
     * Show the form of login
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.login');
    }


    public function logout()
    {
        auth()->logout();
        return redirect()->to('/');
    }
    
}
