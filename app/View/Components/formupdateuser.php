<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\User;

class formupdateuser extends Component
{
    public $action;
    public $user_id;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($action, $user)
    {
        $this->action = $action;
        $this->user_id = $user;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        // return $this->user_id . "gsdfg";
        $user = User::find($this->user_id);
        // return $user;
        return view('components.formupdateuser', compact('user'));
    }
}
