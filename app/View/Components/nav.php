<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Sale;

class nav extends Component
{

    public $number;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($number)
    {
        $this->number = $number;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $user_id = auth()->user()->id;
        $countSale = Sale::whereUser_id($user_id)->count();
        $this->number = $countSale;
        return view('components.nav');
    }
}
