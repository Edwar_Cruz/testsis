<?php

namespace App\View\Components;

use Illuminate\View\Component;

class product extends Component
{
    public $product;
    public $price;
    public $stock;
    public $image;
    public $product_id;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($product,$price, $stock, $image, $id)
    {
        $this->product = $product;
        $this->stock = $stock;
        $this->price = $price;
        $this->image = $image;
        $this->product_id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.product');
    }
}
